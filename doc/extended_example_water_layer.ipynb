{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **YaSoFo example: Counterbalancing  parasitic absorption and ion transport losses in the electrolyte**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, you will learn how to counterbalance the parasitic absorption and ion transport losses in the electrolyte layer of an exemplary PEC water splitting device. The system will be a dual-junction with Si as a bottom absorber and very good catalysts for both the oxygen and the hydrogen evolution reaction. The cathode size is 4 cm, see paper below for geometry assumptions. We calculate the Solar-To-Hydrogen (STH) efficiency as a function of the top absorber bandgap and the water layer thickness. Then, we extract the maximum STH efficiency and the corresponding top absorber band gap and plot the data. For further features and more detailed explanations see the documentation of the \"find_max_double_water_layer\" function in YaSoFo and the following reference: [Kölbach et al., APL 119.8 (2021). DOI:10.1063/5.0060700](https://doi.org/10.1063/5.0060700).\n",
    "Prerequisites: You have installed [SciPy](https://scipy.org/) (Python 3.x version) on your computer and obtained YaSoFo from https://bitbucket.org/YaSoFo/yasofo . You have started a Python 3 shell, such as ipython, in the directory where the file yasofo.py is located."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# change to yasofo root dir:\n",
    "import sys\n",
    "sys.path.append('../')\n",
    "\n",
    "import yasofo as yo # example written for YaSoFo 1.4.0\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "import scipy.constants as phys_const\n",
    "import scipy.special.lambertw as lambertw\n",
    "import collections\n",
    "\n",
    "import pylab as plb\n",
    "import matplotlib.cm as cm\n",
    "import matplotlib.colors as colors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **1) Input parameters**\n",
    "\n",
    "\n",
    "**1.1) General parameters:** <br>\n",
    "\n",
    "> - *water_layer*: Thickness of the water layer in front of the top absorber [cm] <br>\n",
    "> - *ion_transport_losses*: If true, ion transport losses (ohmic and concentration overpotentials) in the water layer are taken into account <br>\n",
    "> - *T_device*: Temperature of the device (both solar cell and electrolyte) [K] <br>\n",
    "\n",
    "\n",
    " **1.2) Solar cell parameters:** <br> \n",
    "> - *spectrum*: The solar spectrum <br>\n",
    "> - *db*: If true, IV characteristics of the solar cells are calculated based on the detailed balance limit <br>\n",
    "> - *real_IV*: If *db* is set to false and *real_IV* is set to true, the modelling is based on an experimental AlGaAs//Si IV curve and the corresponding EQE data <br>\n",
    "> - *allow_thinning*: If true, the top absorber is allowed to be thinned to transmit light to bottom cell and thereby minimise current mismatch <br>\n",
    "> - *U_oc_T_coeff*: Temperature coefficient of the open circiut potential [%/K] <br>\n",
    "\n",
    "\n",
    " **1.3) Catalyst parameters:** <br> \n",
    "> - *OER_catalyst_para*: List of the following four values\n",
    ">> j0_OER_Tref: exchange current density [A/cm²] <br> \n",
    ">> Tref_OER: Reference temperature of the value of jo_OER_Tref [K] <br> \n",
    ">> Ea_OER: Activation Energy OER [J/M] <br> \n",
    ">> Alpha_a_OER: Anodic Charge transfer coefficient multiplied with electrons involved (can be extracted from the tafel slope [ ] <br> \n",
    "           \n",
    "> - *HER_catalyst_para*: List of the following four values\n",
    ">> j0_HER_Tref: exchange current density [A/cm²] <br> \n",
    ">> Tref_HER: Reference temperature of the value of jo_OER_Tref [K] <br> \n",
    ">> Ea_HER: Activation Energy OER [J/M] <br> \n",
    ">> Alpha_a_HER: Cathodic charge transfer coefficient multiplied with electrons involved (can be extracted from the tafel slope [ ] <br> "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "water_layer = np.logspace(-1, 0, num = 10)\n",
    "ion_transport_losses = True\n",
    "T_device = 313.15\n",
    "\n",
    "# Please download a high resolution spectrum, such as the one from NREL (https://www.nrel.gov/grid/solar-resource/spectra.html), for these calculations. \n",
    "# Be aware to use the 'scaling' parameter of the 'solspec' class when importing the NREL spectrum ('scaling=1000').\n",
    "fname_high_res_spectrum = 'None' # set the (filename and path for the high-res spectrum here)\n",
    "if fname_high_res_spectrum == 'None':\n",
    "    print('Warning: No high-res spectrum given! Using standard, low-resolution spectrum.')\n",
    "    spectrum = yo.am15g\n",
    "else:\n",
    "    spectrum = yo.solspec(fname_high_res_spectrum, scaling=1000) # assuming file-format of NREL-spectrum\n",
    "\n",
    "db = False\n",
    "real_IV = True\n",
    "allow_thinning = False\n",
    "U_oc_T_coeff = -0.3\n",
    "fix_bottom = 1.1\n",
    "\n",
    "OER_catalyst_para = [5e-10, 323, 52000, 1.5]\n",
    "HER_catalyst_para = [0.68e-3, 303, 13200, 1.2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **2) Calculations**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Top_bandgap = []\n",
    "\n",
    "STH_eff_example = yo.find_max_double_water_layer(water_layer = water_layer, OER_catalyst_para = OER_catalyst_para , \n",
    "                                                HER_catalyst_para = HER_catalyst_para, T_device = T_device, \n",
    "                                                spectrum=spectrum, fix_bottom = fix_bottom, db=db, real_IV = real_IV, allow_thinning=allow_thinning,\n",
    "                                                ion_transport_losses = ion_transport_losses, U_oc_T_coeff = U_oc_T_coeff)\n",
    "\n",
    "#Considered top absorber bandgap range\n",
    "Top_bandgap.append(STH_eff_example[0][1][6]) \n",
    "\n",
    "\n",
    "#Data for Colorplot: STH vs. water layer thickness & top absorber bandgap\n",
    "efficiencies_matrix = plb.zeros([len(Top_bandgap[0]), len(water_layer)])   \n",
    "for j in range(0, len(Top_bandgap[0])):\n",
    "    for i in range(0, len(water_layer)): \n",
    "        efficiencies_matrix[j,i] = STH_eff_example[i][1][7][j]\n",
    "\n",
    "print('Done!')        \n",
    "#Extracted STH max and corresponding top absorber bandgap\n",
    "STH_max = STH_eff_example[0][2] \n",
    "Top_bandgap_STH_max = STH_eff_example[0][3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  **3) Let's plot the data**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.gridspec as gridspec\n",
    "\n",
    "fig = plt.figure(figsize=(6,9))\n",
    "spec2 = gridspec.GridSpec(ncols=1, nrows=2, figure=fig, width_ratios=[1] , height_ratios=[1, 1])\n",
    "fig_ax1 = fig.add_subplot(spec2[0,0])\n",
    "fig_ax2 = fig.add_subplot(spec2[1,0])\n",
    "\n",
    "labelsize_tick = 15\n",
    "fontsize_label = 21\n",
    "fontsize_legend = 17\n",
    "fontsize_anno = 15\n",
    "linewidth_spectra = 2\n",
    "Colorbar_range_min = 15\n",
    "Colorbar_range_max = 20\n",
    "bbox_props = dict(boxstyle=\"round\", fc=\"white\", lw=0.5, alpha=.9)\n",
    "\n",
    "\n",
    "#Colorplot: STH efficiency as a function of top absorber bandgap and water layer thickness\n",
    "p1= fig_ax1.pcolormesh(water_layer, Top_bandgap[0], efficiencies_matrix, shading='gouraud', cmap = cm.get_cmap('RdYlBu_r'), vmin=Colorbar_range_min, vmax=Colorbar_range_max)\n",
    "fig_ax1.tick_params(axis='x', labelsize = labelsize_tick)\n",
    "fig_ax1.tick_params(axis='y', labelsize = labelsize_tick)\n",
    "fig_ax1.set_xlabel(\"Water layer (cm)\", fontsize=fontsize_label)\n",
    "fig_ax1.set_ylabel(\"$E_{g, Top}$ (eV)\", fontsize=fontsize_label)  \n",
    "fig_ax1.set_xscale(\"log\") \n",
    "fig_ax1.set_ylim(1.6, 2.0)\n",
    "fig_ax1.set_xlim(0.1, 1)\n",
    "fig_ax1.xaxis.set_minor_formatter(plt.NullFormatter())\n",
    "xmin, xmax = fig_ax1.get_xlim()\n",
    "fig_ax1.set_xticks(np.round(np.linspace(xmin, xmax, 4), 2))\n",
    "text1 = fig_ax1.text(0.107, 1.965, 'Bottom absorber: Si; cathode size: 4 cm', size=fontsize_anno, bbox=bbox_props)\n",
    "\n",
    "#Extracted STH max and corresponding top absorber bandgap\n",
    "ins3 = fig_ax2.plot(water_layer, STH_max, color = \"indianred\", label = \"$\\\\eta_{STH, max}$\", linewidth = 3)                  \n",
    "fig_ax2.set_xlabel('Water layer (cm)', fontsize = fontsize_label)\n",
    "fig_ax2.set_ylabel('$\\\\eta_{STH, max}$ (%)', fontsize = fontsize_label, color = \"indianred\")\n",
    "fig_ax2.set_xscale(\"log\") \n",
    "fig_ax2.set_ylim(17.5, 21.5)\n",
    "fig_ax2.set_xlim(0.1, 1)\n",
    "fig_ax2.tick_params(axis='x', labelsize = labelsize_tick)\n",
    "fig_ax2.tick_params(axis='y', labelsize = labelsize_tick)\n",
    "fig_ax2.xaxis.set_minor_formatter(plt.NullFormatter())\n",
    "xmin, xmax = fig_ax2.get_xlim()\n",
    "fig_ax2.set_xticks(np.round(np.linspace(xmin, xmax, 4), 2))\n",
    "text2 = fig_ax2.text(0.107, 21.1, 'Extracted $\\\\eta_{STH, max}$ and $E_{g, Top}$', size=fontsize_anno, bbox=bbox_props)\n",
    "eg_ax2 = fig_ax2.twinx()\n",
    "ins4 = eg_ax2.plot(water_layer, Top_bandgap_STH_max, color = \"steelblue\", label = \"Optimized $E_{g}$\", linewidth= 3)\n",
    "eg_ax2.set_ylim(1.65, 1.85)\n",
    "eg_ax2.tick_params(axis='y', labelsize = labelsize_tick)\n",
    "eg_ax2.set_ylabel('$E_{g, Top}$ (eV)', fontsize = fontsize_label, color = \"steelblue\")\n",
    "\n",
    "plt.tight_layout()\n",
    "\n",
    "#Add Colorbar\n",
    "plt.subplots_adjust(hspace=0.3)\n",
    "fig.subplots_adjust(right=0.94)\n",
    "cbar_ax_1 = fig.add_axes([0.96, 0.59, 0.05, 0.385])\n",
    "cbar = plt.colorbar(p1, cax=cbar_ax_1,  orientation=\"vertical\")\n",
    "cbar.set_label(label='$\\\\eta_{STH}$ (%)', size= fontsize_label)\n",
    "cbar.ax.tick_params(labelsize=labelsize_tick)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

# -*- coding: utf-8 -*-
"""
Created on Fri Aug 13 14:50:31 2021

@author: Moritz Koelbach
"""

#Additional packages:

from numpy import genfromtxt, interp, where, meshgrid, array, linspace, exp, average, arange
import os.path
from scipy.optimize import curve_fit
from scipy.interpolate import interp2d
from src.spectrum import solspec, am15g, mod_spectrum_by_abs, mod_spectrum, nm_to_ev
import scipy.constants as phys_const
import yasofo as yo


volt_loss_water_thickness = os.path.join(os.path.dirname(__file__), 'voltage_loss_water_layer_thickness.dat')
EQE_dual_junction_total = os.path.join(os.path.dirname(__file__), 'EQE_AlGaAs_Si_no12578_x636-11.dat')
IV_dual_junction = os.path.join(os.path.dirname(__file__), 'IV_AlGaAs_Si_no12578_x636-11.dat')


def find_max_double_water_layer(water_layer, OER_catalyst_para, HER_catalyst_para,
                                T_device, pH = 0,
                                spectrum=am15g, power_E0=True, E0=1.23,
                                allow_thinning=True,  nid=1.0,
                                db=True, fix_bottom = 1.1, real_IV = False,
                                voltage_loss=0.4,
                                ion_transport_losses = True,
                                Egtopref = 1.75, Egbottomref = 1.1,
                                U_oc_T_coeff = 0, Electrolyte_para = None,
                                d = None):

    """Maximizes the efficiency of a dual-junction water splitting device with
    a fixed bottom cell bandgap while counterbalancing the parasitic absorption
    and ion transport losses in the water layer.

    Returns [[Eg_hi, Eg_lo, j, U_oc], [Eg_hi, Eg_lo, j, P, U_oc,
    min_photo_current, top_bandgap, STH_eff, cat_without_losses], STH max,
    Higher gap max] for each water layer.


    Parameters
    ----------
    water_layer: float
        Water layer thickness in cm.
    OER_catalyst_para: list of floats
        OER Catalyst parameters: [j0_OER_Tref: exchange current density in
                                  Acm^-2,
                                  Tref_OER: Temperature in K,
                                  Ea_OER: Activation Energy OER in J/M,
                                  alpha_a_OER: Anodic Charge transfer
                                  coefficient multiplied with electrons involved
                                  (can be extracted from the tafel slope]
    HER_catalyst_para: list of floats
        HER Catalyst parameters: [j0_HER_Tref: exchange current density in
                                 Acm^-2,
                                 Tref_HER: Temperature in K,
                                 Ea_HER: Activation Energy OER in J/M,
                                 alpha_a_HER * z: Cathodic charge transfer
                                 coefficient multiplied with electrons involved
                                 (can be extracted from the tafel slope)]
    T_device: float
        Temperature of the device (both solar cell and electrolyte) in K
    pH: float
        pH value of the electrolyte

    spectrum: solspec
        The AM1.5G solar spectrum (1000 W/m²)
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.

    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    fix_bottom: float
        Band gap energy of the botton absorber in eV (1.1 for Si).
    real_IV: bool
        If "db" is set to false and "real_IV is set to true, the modelling is
        based on an experimental AlGaAs//Si EQE data and IV curve.
    voltage_loss: float
        If "db" and "real_IV" are set to false, "voltage_loss" is
        used to estimate the open circut potential. The value presents the
        photovoltage loss per junction compared to the bandgap in V.
    ion_transport_losses: bool
        Shall we include the ion transport losses in the water layer to model
        the IV characteristics of the catalysts? If so, the results of a
        COMSOL simulations are used as an input file for a cathode size of
        4 cm. For details see the following publication: XXX
    Egtopref:
        blabla
    Egbottomref:
        blabla
    U_oc_T_coeff:
        Temperature coefficient [%/K] of the open circuit voltage.
    Electrolyte_para: list of floats
        If "ion_transport_losses" is false (see below), these values are used
        to obtain an ohmic drop based on the conductivity of the electrolyte
        and the distance d (see below) of the electrodes.
        Electrolyte parameter: [y0: y-intercept,
                                m: slope (Temperature-dependence
                               electrolyte according to
                               Conductivity = y0 + m*T:_device in [S/cm])]
    d: float
        Distance of the tow catalysts in cm. As described above, this value is
        used if "ion_transport_losses" is not used.
    """

    if ion_transport_losses is True:

        volt_loss_matrix = genfromtxt(volt_loss_water_thickness, skip_header = 3)
        current_dens_ref = volt_loss_matrix[0,:][1:]
        water_layer_ref= volt_loss_matrix[:,0][1:]

        X, Y = meshgrid(current_dens_ref, water_layer_ref)
        Z = []

        for i in range(1, len(water_layer_ref)+1):
            Z_i = volt_loss_matrix[i,:][1:]
            Z.append(Z_i)


        y2 = array(water_layer)
        current_dens_ext = linspace(0, max(current_dens_ref), 100)
        f = interp2d(current_dens_ref, water_layer_ref, Z)

        X2, Y2 = meshgrid(current_dens_ext, y2)
        Z2 = f(current_dens_ext, y2)

    results = []
    STH_eff_max = []  #max STH for each wavelength
    Higher_gap_max = []  #Corresponding bandgap of STH max
    mod_spectra = []
    IV_cat_water_layer = []

    for thickness in water_layer:
        spectra = mod_spectrum_by_abs(thickness=thickness, spectrum=spectrum)
        if real_IV is True:
            spectra = mod_spectrum(EQE_dual_junction_total, spectrum=spectra, header=3)

        mod_spectra.append(spectra)


    if real_IV is True:
        #Get empirical diode fit parameters C1 and C2:
        IV_dual_junction_real = genfromtxt(IV_dual_junction, skip_header = 3)
        jsh_ref = IV_dual_junction_real[:,1][0]
        V_oc_ref =IV_dual_junction_real[:,0][-1]

        x, y = IV_dual_junction_real[:,0], IV_dual_junction_real[:,1]

        def fit_func(x, C1, C2):
            return jsh_ref*(1-C1*(exp(x/(C2*V_oc_ref))-1))

        # curve fit
        popt, _ = curve_fit(fit_func, x, y)
        C1, C2 = popt


    water_layer_count = range(0, len(water_layer))


    for spectrum, n in zip(mod_spectra, water_layer_count):

        print('I am calculating the ' + str(n +1) + '. water layer')
        current = 0.0
        photo_current = [0.0, 0.0]
        power = 0.0
        max_gap = nm_to_ev(min(spectrum.data[:, 0]))
        # we store the overall best combination regarding the current/power here
        best_combination_current = [0.0, 0.0, 0.0, 0.0]
        best_combination_power = [0.0, 0.0, 0.0, 0.0, 0.0]

        higher_lambda = nm_to_ev(fix_bottom)

        top_bandgap = []
        STH_eff = []


        i = where(spectrum.data[:, 0] < higher_lambda)[0][-1]
        for lower_lambda in spectrum.data[:i, 0]:


            higher_gap = nm_to_ev(lower_lambda)
            lower_gap = nm_to_ev(higher_lambda)

            top_bandgap.append(higher_gap)

                    # bottom cell
            photo_current[1] = spectrum.integrate_flux(higher_lambda,
                    lower_lambda, electronvolts=False)
                    # top cell
            photo_current[0] = spectrum.integrate_flux(lower_lambda,
                                 spectrum.data[0, 0], electronvolts=False)
                    # current matching condition
            min_photo_current = min([photo_current[0], photo_current[1]])
            if (allow_thinning is True) and (photo_current[0] > photo_current[1]):
                        # make top cell thinner to transmit light to bottom cell
                average_current = average(photo_current)
                min_photo_current = average_current
                photo_current[0] = photo_current[1] = average_current
            if db is False:

                if real_IV is False:

                    if lower_gap < 2*voltage_loss:
                        U_oc = higher_gap + lower_gap/2 - voltage_loss
                    else:
                        U_oc = higher_gap + lower_gap - 2*voltage_loss

                else:

                    IV_dual_junction_real = genfromtxt(IV_dual_junction, skip_header = 3)

                    V_oc_ref = IV_dual_junction_real[:,0][-1]
                    loss_real = Egtopref + Egbottomref - V_oc_ref
                    U_oc = (higher_gap + lower_gap) - loss_real


            else:
                U1 = yo.Voc_det_balance(higher_gap, spectrum, max_gap,
                                     nid=nid, temperature = 298.15, photo_current=photo_current[0])
                U2 = yo.Voc_det_balance(lower_gap, spectrum, higher_gap,
                                     nid=nid, temperature = 298.15, photo_current=photo_current[1])
                U_oc = U1 + U2

            U_oc= U_oc + 0.01*U_oc_T_coeff*(T_device-298.15)*U_oc

            if power_E0 is False:
                [power, U_mpp, current] = yo.mpp_single_diode_current(min_photo_current,
                    U_oc, nid, temp=300)


            E0_OER = 1.23 + (T_device-298.15)*-0.0008456 #Temperature dependence E0_OER
            E0_HER = 0
            E_OER_N = E0_OER + ((phys_const.R*T_device)/(4*phys_const.value
            ('Faraday constant')))*-2.303*4*pH
            E_HER_N = E0_HER + ((phys_const.R*T_device)/(2*phys_const.value
            ('Faraday constant')))*-2.303*2*pH
            j0_OER = OER_catalyst_para[0]*exp((OER_catalyst_para[2]/phys_const.R)*
            ((1/OER_catalyst_para[1])-(1/T_device)))
            j0_HER = HER_catalyst_para[0]*exp((HER_catalyst_para[2]/phys_const.R)*
            ((1/HER_catalyst_para[1])-(1/T_device)))

            # stepsize for voltage range (impacts results for low j)
            E_appl_OER = arange(E_OER_N, 3, 0.00001)  # [V]
            E_appl_HER = arange(-1, E_HER_N, 0.00001) # [V]

            j_OER = j0_OER*(exp(OER_catalyst_para[3]*phys_const.value
            ('Faraday constant')*(E_appl_OER-E_OER_N)/(phys_const.R*T_device))) #[A/cm²]
            j_HER = j0_HER*(-exp(-(HER_catalyst_para[3])*phys_const.value
            ('Faraday constant')*(E_appl_HER-E_HER_N)/(phys_const.R*T_device))) #[A/cm²]

            # Calculation of 2-Electrode IU
            current_range_OER = linspace(0.00001, 1, 10000)#[A/cm²]
            current_range_HER = linspace(-1, -0.00001, 10000)#[A/cm²]
            Voltage_OER = interp(current_range_OER, j_OER, E_appl_OER) #[V]
            Voltage_HER = interp(current_range_HER, j_HER, E_appl_HER) #[V]
            Voltage_HER_reverse = Voltage_HER[::-1] #[V]
            U = Voltage_OER - Voltage_HER_reverse #[V]


            if ion_transport_losses == False:

                conductance_electrolyte = Electrolyte_para[1] + Electrolyte_para[2]*T_device  #[S/cm]
                voltage_loss = (current_range_OER*d)/(conductance_electrolyte) #[V]
                U = U + voltage_loss #[V]


            cat_current = current_range_OER
            cat_without_losses = [U,cat_current]


            if ion_transport_losses is True:

                #Interpolation of voltage loss mit beiden current densitiy
                Voltage_loss_int =  interp(cat_current*1000, current_dens_ext, Z2[n])
                U_loss = U + Voltage_loss_int

                cat_ohmic_conc = [U_loss, cat_current]

            if real_IV is True:

                diode_current = min_photo_current * (1-C1*(exp(U/(C2*U_oc))-1))


            if real_IV is False:
                U_T = phys_const.k*T_device/phys_const.e
                diode_current = min_photo_current*(1-(exp(U/(nid*U_T))-1)/((exp(U_oc/(nid*U_T))-1)))
                    #print(min_photo_current)


            if ion_transport_losses is True:

                diode_inter_to_Uloss =  interp(U_loss, U, diode_current)

                curr_element = where(abs(diode_inter_to_Uloss -
                            cat_current) == min(abs(diode_inter_to_Uloss
                            - cat_current)))[0][0]
                current = diode_inter_to_Uloss[curr_element]

            if ion_transport_losses is False:

                curr_element = where(abs(diode_current - cat_current) ==
                                    min(abs(diode_current - cat_current)))[0][0]
                current = diode_current[curr_element]


            if current > best_combination_current[2]:
                best_combination_current = [higher_gap, lower_gap, current, U_oc]

            if U_oc < E0:
                power = 0.0
                STH_eff.append(power)
            else:
                power = current*E0
                STH_eff.append(power*1000)
                if power > (best_combination_power[3]):
                    best_combination_power = [higher_gap, lower_gap, current, power, U_oc, min_photo_current, top_bandgap, STH_eff, cat_without_losses ]

        STH_eff_max.append(best_combination_power[3]*1000)
        Higher_gap_max.append(best_combination_power[0])


        res_water_layers = [best_combination_current, best_combination_power, STH_eff_max, Higher_gap_max]


        if ion_transport_losses is True:
            IV_cat_water_layer.append(cat_ohmic_conc)
            res_water_layers.append(IV_cat_water_layer)


        results.append(res_water_layers)

    return results

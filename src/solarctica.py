# -*- coding: utf-8 -*-
# This source code is part of YaSoFo, distrubuted
# under the EUPL v1.2 license. See 'LICENSE' for further
# information

import numpy as np
import scipy.constants as phys_const
from sympy import var, solve
from src.spectrum import solspec, change_spec_resolution, am15g, mod_spectrum
from src.spectrum import mod_spectrum_by_abs, nm_to_ev, ev_to_nm
import matplotlib.pyplot as plt
import collections
import sys


def calc_device_t(T_out, t, abso, area_solar_cells_total, C, I0,
                       j_op, E_op, e_solarcell, area_housing,
                       e_housing, h_solarcell_air, h_housing_air, E_th = 1.48,
                       l_Ins = None, k_Ins = None, thermal_coupling = True):
    """Calculates the device temperature of closely coupled water
    splitting device. Adapted model from Min et al., J. Semicond. 2009, 30(4),
    044011.

    Returns the resulting temperature (float) of the device in an ordered
    dictionairy containing the key:

        * T_device - Temperature in [K]

    Parameters
    ----------
    T_out: float
        Outdoor temperature in [K]
    t: float
        Transmissivity of the optical collector []
    abso: float
        Surface absorptivity of the cell []
    area_solar_cells_total: float
        Total area of the solar cells [m²]
    C: float
        Concentration factor []
    I0: float
        Irradiance [W/m²] (1000 W/m² for AM1.5)
    j_op: float
        operation current density of the solar cell [A/m²]
    E_th: float
        Thermoneutral Potential for water splitting [V]
    E_op: float
        Operation Potential of the device [V]
    e_solarcell: float
        Emmisivity of the solar cells []
    area_housing: float
        Area of the device housing [m²]
    e_housing: float
        Emmisivity of the device housing or insulation (radiant barrier) in
        the infrared regiono [].
    h_solarcell_air: float
        Convective transfer coefficient solar cell - air [W/(m²*K)]
    h_housing_air: float
        Convective transfer coefficient device housing - air [W/(m²*K)]
    l_Ins: float
        Thickness of Insulation around the housing [m]
    k_Ins: float
        Thermal conductivity of Insulation around the housing [W/(m*K)]
    thermal_coupling: boolian
        if true: Temperature of the a thermally coupled water splitting device
                is calculated  (i.e., solar cells & electrolyte) based on the
                device housing parameters
        if false: Solar cell temperature is estimated assuming a total area
                of 2*area_solar_cells_total availabe for heat dissipation.

    """

    if C == 1:
        t = 1

    if l_Ins ==None:
        Ut = h_housing_air

    else:
        Ut = ((l_Ins/k_Ins)+ 1/h_housing_air)**-1


    if thermal_coupling == True:

        T_device = var("T_device")
        T_calc = solve(t*abso*area_solar_cells_total*C*I0-(j_op*E_op/(C*I0))
                       *(E_th/E_op)*t*abso*area_solar_cells_total*C*I0-
                       (area_solar_cells_total*
                       e_solarcell*phys_const.Stefan_Boltzmann*
                       (T_device**4-T_out**4) + area_housing*
                       e_housing*phys_const.Stefan_Boltzmann*
                       (T_device**4-T_out**4) )-(area_solar_cells_total*
                        h_solarcell_air*(T_device-T_out)+
                        area_housing*Ut*
                        (T_device-T_out)))

        T_device_calc = T_calc[1]
        if T_device_calc >= 373.15:
            print('Attention: Device T > 100°C')

    if thermal_coupling == False:

        T_device = var("T_device")
        T_calc = solve(t*abso*area_solar_cells_total*C*I0-(j_op*E_op/(C*I0))
                       *t*abso*area_solar_cells_total*C*I0-
                       (2*area_solar_cells_total*
                       e_solarcell*phys_const.Stefan_Boltzmann*
                       (T_device**4-T_out**4) )-(2*area_solar_cells_total*
                        h_solarcell_air*(T_device-T_out)
                        ))

        T_device_calc = T_calc[1]
        if T_device_calc >= 373.15:
            print('Attention: Device T > 100°C')

    Result_Dict = collections.OrderedDict()
    Result_Dict['T_device'] = T_device_calc

    return Result_Dict


def get_JV_multijunction_det_balance(gaps, T, nid, C, r_ind=[1, 0],
                                     spectrum=am15g):
    """ Calculates the IV curve of a double junction solar cell as a function
    of the absorber bandgaps and temperature.
    Returns the resulting IV curve in an ordered dictionairy containing the
    keys:

        * J - Current density in A/cm²
        * V - Voltage in V

    Parameters
    ----------
    T: float
       Temperature in [K]
    gaps = list of floats
        bandgap of the absorbers (from the highest to the lowest gap)
    nid = float
        diode ideality factor (between 1 and 2)
    C: float
        Concentration factor []
    spectrum: solspec
        The spectrum.
    r_ind: list of floats
        Refractive indices of medium over and under the junction [n_o, n_u]

    """

    #Calculation of dark saturation current density in the det. balance limit
    #(adapted from Yasofo "Voc_det_balance"):
    kT = phys_const.k*T
    ri_factor = r_ind[0]**2 + r_ind[1]**2

    j_0_list = []
    jsh_list = []

    for gap in gaps:

        gapSI = gap*phys_const.e
        # Boltzmann approximation (factor 10k from m2 to cm2):
        j_0 = (2*np.pi*phys_const.e*ri_factor/(phys_const.h**3*phys_const.c**2)
               *kT*(gapSI**2 + 2*kT*gapSI + 2*(kT)**2))*np.exp(-gapSI/kT)/10000
        j_0n = j_0**(1/nid)
        j_0_list.append(j_0n)
    #Calculation of photocurrent in each absorber (EQE = 1, "infinite
    #absorber thickness")

    #TODO: implement T-dependency of the bandgaps

    photo_current_gap1 = spectrum.integrate_flux(100, gaps[0],
                                                 electronvolts=True)*C
    photo_current_gap2 = spectrum.integrate_flux(gaps[0], gaps[1],
                                                 electronvolts=True)*C
    jsh_list.append(photo_current_gap1)
    jsh_list.append(photo_current_gap2)

    if len(gaps) == 3:
        photo_current_gap3 = spectrum.integrate_flux(gaps[1], gaps[2],
                                                     electronvolts=True)*C
        jsh_list.append(photo_current_gap3)

    #Limiting photocurrent:
    J_min = min(jsh_list)

    #Calculation of JV curve
    J_range = np.linspace(0, J_min, 1000)
    V_list = []

    for J in J_range:
        if len(gaps) == 2:
            V = (phys_const.k*T/phys_const.e)*(nid*np.log(((J_min-J)/
            j_0_list[0])+1) + nid*np.log(((J_min-J)/j_0_list[1])+1))
            V_list.append(V)
        elif len(gaps) == 3:
            V = (phys_const.k*T/phys_const.e)*(nid*np.log(((J_min-J)/
            j_0_list[0])+1) + nid*np.log(((J_min-J)/j_0_list[1])+1) +
            nid*np.log(((J_min-J)/j_0_list[2])+1))
            V_list.append(V)
    J_range_reverse = J_range[::-1]
    V_list_reverse = V_list[::-1]
    Result_Dict = collections.OrderedDict()
    Result_Dict['V'] = V_list_reverse
    Result_Dict['J'] = J_range_reverse

    return Result_Dict


def estimate_JV_from_datasheet(T, C, T_ref, Isc_Tref, Isc_T_coeff, U_oc_Tref,
                               U_oc_T_coeff, nid=2):
    """ Estimates the IV curve of a solar cell as a function of the
    temperature and light
    Concentration based on the manufacturer's datasheet.
    Returns the resulting IV curve in an ordered dictionairy containing the
    keys:

        * J - Current density in  A/cm²
        * V - Voltage in V

    Parameters
    ----------
    T: float
       Temperature in [K]
    C = float
        Concentration factor
    T_ref = float
        Reference temperature in [K] of the T-coefficients of the the short
        current density
        and open circuit potential
    Isc_Tref = float
        Short current density in [mA/cm²] at the reference temperature
    Isc_T_coeff = float
        Temperature coefficient [%/K] of the short current density
    U_oc_Tref = float
        Open circuit voltage in [V] at the reference temperature
    U_oc_T_coeff = float
         Temperature coefficient [%/K] of the open circuit voltage
    nid = float
        diode ideality factor (between 1 and 2); Can be estimated from the
        open circuit
        potential as a function of C from the datasheet
    """

    U = np.arange(0, U_oc_Tref+1, 0.001)
    U_T = phys_const.k*T/phys_const.e

    #Influence of T on Isc and U_oc:
    Isc_T = Isc_Tref + 0.01*Isc_T_coeff*(T-T_ref)*Isc_Tref
    U_oc_T = U_oc_Tref + 0.01*U_oc_T_coeff*(T-T_ref)*U_oc_Tref


    #Infleunce of light concentration on Isc and U_oc:
    Isc_T_C = Isc_T*C
    U_oc_T_C = U_oc_T + nid*U_T*np.log(C)

    diode_current = 0.001*Isc_T_C*(1-(np.exp(U/(nid*U_T))-1)/
                    ((np.exp(U_oc_T_C/(nid*U_T))-1))) #in A/cm²

    Result_Dict = collections.OrderedDict()
    Result_Dict['V'] = U
    Result_Dict['J'] = diode_current

    return Result_Dict


def match_catalysts_solar_cell_current(T_device, OER_catalyst_para, HER_catalyst_para,
                                       Electrolyte_para, Configuration_para, C,
                                       gaps = None, nid =
                                       None, r_ind=[1, 0], spectrum=am15g,
                                       T_ref = None, Isc_Tref = None,
                                       Isc_T_coeff = None,
                                       U_oc_Tref = None, U_oc_T_coeff = None,
                                       method = None,
                                       thermal_coupling = True,
                                       T_solarcell = None):

    """Matches the temperature-dependent IV characteristic to the temperature-
    dependent CV (+ohmic drop of the electrolyte) of a HER and OER catalysts.
       This function allows different methods for getting the IV curve of the
       solar cell:

            "datasheet": Estimates IV curve based on the manufacturer's
            datasheet (see estimate_JV_from_datasheet function)

            "det_balance": Calcuates IV curve based on the detailed balance
            limit (see get_JV_multijunction_det_balance function)


    Returns the operation current density, operation voltage, resulting IV
    curve in an ordered dictionairy containing the following keys:


        * jop - Operation current density in [A/m²], float
        * Vop - Operation Voltage in [V], float
        * IV_catalysts - IV characeristics of the catalyts.List of two lists:
            Voltage [V] and Current [A] (absolute current)
        * IV_solar_cells - IV characeristics of the solar cell. list of two
            lists: Voltage [V] and Current [A/cm²]

       Parameters
       ----------
       T_device: float
           Device Temperature in K

       OER_catalyst_para: list of floats
           OER Catalyst parameters: [j0_OER_Tref: exchange current density in
                                     Acm^-2,
                                     Tref_OER: Temperature in K,
                                     Ea_OER: Activation Energy OER in J/M,
                                     alpha_a_OER: Anodic Charge transfer
                                     coefficient multiplied with electrons involved
                                     (can be extracted from the tafel slope]

       HER_catalyst_para: list of floats
           HER Catalyst parameters: [j0_HER_Tref: exchange current density in
                                     Acm^-2,
                                     Tref_HER: Temperature in K,
                                     Ea_HER: Activation Energy OER in J/M,
                                     alpha_a_HER * z: Cathodic charge transfer
                                     coefficient multiplied with electrons involved
                                     (can be extracted from the tafel slope)]

       Electrolyte_para: list of floats
           Electrolyte parameter: [pH: pH value of electrolyte,
                                   y0: y-intercept,
                                   m: slope (Temperature-dependence
                                   electrolyte according to
                                   Conductivity = y0 + m*Temperature[S/cm])]


       Configuration_para: List of floats
           Configuration parameter: [area_solar_cell: Area of solar cells in
                                     m^2,
                                     area_ratio: Ratio of Area catalysts/Area
                                     solar cells,
                                     distance: distance of electrodes in cm,
                                     geometry_const: geometry constant of the
                                     cell accounting for fringing
                                     effects with respect to the iR-drop
                                     in the electrolyte ( ≥1; 1 for no
                                     fringing effects)

         C = float
            Concentration factor

        method: string ("datasheet" or "det_balance")
            Decides which method is used to get the IV curve of the solar cell:
            Can be "datasheet"(estimate_JV_from_datasheet-function) or
            "det_balance" (get_JV_multijunction_det_balance-function)

        Additional input parameters required for the det_balance method:

        gaps = list of floats
           bandgap of the absorbers (from the highest to the lowest gap)

       spectrum: solspec
           The spectrum.

       r_ind: list of floats

           Refractive indices of medium over and under the junction [n_o, n_u]

       Additional input parameters required for the datasheet method:

       T_ref = float
            Reference temperature in [K] of the T-coefficients of the the
            short current density
            and open circuit potential
       Isc_Tref = float
            Short current density in [mA/cm²] at the reference temperature
       Isc_T_coeff = float
            Temperature coefficient [%/K] of the short current density
       U_oc_Tref = float
            Open circuit voltage in [V] at the reference temperature
       U_oc_T_coeff = float
             Temperature coefficient [%/K] of the open circuit voltage
       nid = float
            diode ideality factor (between 1 and 2); Can be estimated from the
            open circuit potential as a function of light from the datasheet

       thermal_coupling: boolian
          Decides whether the system is thermally coupled or not
          (see "get_STH_eff"-function for further definitons)

       T_solarcell: float
           Temperature of the solar cells (if thermal_coupling = False;
           see "get_STH_eff"-function for further definitons)

       """

    E0_OER = 1.23 + (T_device-298.15)*-0.0008456 #Temperature dependence E0_OER
    E0_HER = 0
    E_OER_N = E0_OER + ((phys_const.R*T_device)/(4*phys_const.value
    ('Faraday constant')))*-2.303*4*Electrolyte_para[0]
    E_HER_N = E0_HER + ((phys_const.R*T_device)/(2*phys_const.value
    ('Faraday constant')))*-2.303*2*Electrolyte_para[0]
    j0_OER = OER_catalyst_para[0]*np.exp((OER_catalyst_para[2]/phys_const.R)*
    ((1/OER_catalyst_para[1])-(1/T_device)))
    j0_HER = HER_catalyst_para[0]*np.exp((HER_catalyst_para[2]/phys_const.R)*
    ((1/HER_catalyst_para[1])-(1/T_device)))

    # stepsize for voltage range  <- this impacts results for low j
    E_appl_OER = np.arange(E_OER_N, 3, 0.001)  # [V]
    E_appl_HER = np.arange(-1, E_HER_N, 0.001) # [V]

    j_OER = j0_OER*(np.exp(OER_catalyst_para[3]*phys_const.value
    ('Faraday constant')*(E_appl_OER-E_OER_N)/(phys_const.R*T_device))) #[A/cm²]
    j_HER = j0_HER*(-np.exp(-(HER_catalyst_para[3])*phys_const.value
    ('Faraday constant')*(E_appl_HER-E_HER_N)/(phys_const.R*T_device))) #[A/cm²]

    # Calculation of 2-Electrode IU
    current_range_OER = np.linspace(0.0001, 1, 100000)#[A/cm²]
    current_range_HER = np.linspace(-1, -0.0001, 100000)#[A/cm²]
    Voltage_OER = np.interp(current_range_OER, j_OER, E_appl_OER) #[V]
    Voltage_HER = np.interp(current_range_HER, j_HER, E_appl_HER) #[V]
    Voltage_HER_reverse = Voltage_HER[::-1] #[V]
    Voltage_total = Voltage_OER - Voltage_HER_reverse #[V]

    #Ohmic losses for 2-electrode setup
    conductance_electrolyte = Electrolyte_para[1] + Electrolyte_para[2]*T_device
    R_cell = (1/Configuration_para[3])*Configuration_para[2] / (Configuration_para[1]*Configuration_para[0]*10000*conductance_electrolyte) #[Ohm]
    voltage_loss = current_range_OER*Configuration_para[1]*Configuration_para[0]*10000*R_cell
    Voltage_total_iR = Voltage_total + voltage_loss #[V]
    current_absolute_catalysts = current_range_OER*Configuration_para[1]*Configuration_para[0]*10000 #[A]
    IV_catalysts = [Voltage_total_iR, current_absolute_catalysts]

    #get_JV_doublejunction_det_balance function:
    if thermal_coupling == True:

        if method == 'det_balance':

            if gaps == None:
                sys.exit('Please use the bandgaps as input parameters')
            elif nid == None:
                sys.exit('''Please use the diode ideality factor as an input
                         parameter''')

            my_IV = get_JV_multijunction_det_balance(gaps, T_device, nid, C)

        if method == 'datasheet':

            if T_ref == None:
                sys.exit('''Please use T_ref as an input parameter for the
                         match_catalysts_solar_cell_current function''')
            elif Isc_Tref == None:
                sys.exit('''Please use Isc_Tref as an input parameter for the
                         match_catalysts_solar_cell_current function''')
            elif Isc_T_coeff == None:
                sys.exit('''Please use Isc_T_coeff as an input parameter for
                         the
                         match_catalysts_solar_cell_current function''')
            elif U_oc_Tref == None:
                sys.exit('''Please use U_oc_Tref as an input parameter for the
                         match_catalysts_solar_cell_current function''')
            elif U_oc_T_coeff == None:
                sys.exit('''Please use U_oc_Tref as an input parameter for the
                         match_catalysts_solar_cell_current function''')
            elif nid == None:
                sys.exit('''Please use nid as an input parameter for the
                         match_catalysts_solar_cell_current function''')


            my_IV = estimate_JV_from_datasheet(T_device, C, T_ref, Isc_Tref,
                                               Isc_T_coeff,
                                               U_oc_Tref, U_oc_T_coeff, nid)
        elif method == None:
            raise ValueError('''Please choose a method: det_balance,
                             datasheet...''')
    elif thermal_coupling == False:
        if method == 'det_balance':

                if gaps == None:
                    sys.exit('Please use the bandgaps as input parameters')
                elif nid == None:
                    sys.exit('''Please use the diode ideality factor as an
                             input parameter''')

                my_IV = get_JV_multijunction_det_balance(gaps, T_solarcell,
                                                         nid, C)
        if method == 'datasheet':

                if T_ref == None:
                    sys.exit('''Please use T_ref as an input parameter for the
                             match_catalysts_solar_cell_current function''')
                elif Isc_Tref == None:
                    sys.exit('''Please use Isc_Tref as an input parameter for
                             the
                             match_catalysts_solar_cell_current function''')
                elif Isc_T_coeff == None:
                    sys.exit('''Please use Isc_T_coeff as an input parameter
                             for the
                             match_catalysts_solar_cell_current function''')
                elif U_oc_Tref == None:
                    sys.exit('''Please use U_oc_Tref as an input parameter
                             for the
                             match_catalysts_solar_cell_current function''')
                elif U_oc_T_coeff == None:
                    sys.exit('''Please use U_oc_Tref as an input parameter
                             for the
                             match_catalysts_solar_cell_current function''')
                elif nid == None:
                    sys.exit('''Please use nid as an input parameter for the
                             match_catalysts_solar_cell_current function''')
                my_IV = estimate_JV_from_datasheet(T_solarcell, C, T_ref,
                                                   Isc_Tref, Isc_T_coeff,
                                                   U_oc_Tref, U_oc_T_coeff,
                                                   nid)
    IV_solar_cell = [my_IV['V'], my_IV['J']] #[V] and [A/cm²]

    Solar_cell_Current_abs = 10000*Configuration_para[0]*np.interp(Voltage_total_iR, my_IV['V'], my_IV['J']) #[A]

    # find the voltage, where the difference between catalyst current and
    j_op_index = np.where(abs(Solar_cell_Current_abs - current_absolute_catalysts)
    == min(abs(Solar_cell_Current_abs - current_absolute_catalysts)))[0][0]
    j_op = (current_absolute_catalysts[j_op_index]) #[A]
    V_op = (Voltage_total_iR[j_op_index]) #[V]

    Result_Dict = collections.OrderedDict()
    Result_Dict['j_op'] = j_op/Configuration_para[0] #A/m²
    Result_Dict['V_op'] = V_op
    Result_Dict['IV_catalysts'] = IV_catalysts #[V] and [A]
    Result_Dict['IV_solar_cells'] = IV_solar_cell #[V] and [A/cm²]

    return Result_Dict


def itertation_func_general(func1, func1_iteration_para,
                            func2, func2_iteration_para,
                            max_iter, delta,
                            Key_func1, Key_func2, additional_output = None):

    '''This function iteratively solves an implicit problem of two functions
    that depend on eacht other.

     Returns the resulting converging parameters in ordered dictionairy
     containing the keys:


        * Number_of_iterations - list counting the number of iterations needed
                                  until convergence is reached

        * Iteration_parameter_func1 - Converging parameters of function 1
                                    (orderd
                                    dictionary with keyword 'parai', where i
                                    is the index of the input parameter)

        * Iteration_parameter_func2 - Converging parameters of function 2
                                    (orderd
                                    dictionary with keyword 'parai', where i
                                    is the index of the input parameter)

     Parameters
     ----------
     func1: function

     func2: function

     func1_iteration_para: list of floats
         Defines which parameter(s) of function 1 are used for the iteration

     func2_iteration_para: list of floats
         Defines which parameter(s) of function 2 are used for the iteration

     max_iter: integer
         Maximal number of iteration until convergence is reached

     delta: float
          Defines when convergence is reached and the loop is stopped

     Key_func1: ['String']
         Keyword(s) of the "func1_iteration_para" parameters in the result
         dictionairy of function 1

     Key_func2: ['String']
         Keyword(s) of the "func2_iteration_para" parameters in the result
         dictionairy of function 2
    '''

    #This creates a dictionairy for func2 iteration parameters to check for convergence:
    func2_iteration_para_dict = {}
    for i in range(0, len(func2_iteration_para)):
        func2_iteration_para_dict['para'+str(i)] = []

    #This puts the first guessed value(s) in the dicts:
    for i in range(0, len(func2_iteration_para)):
        	func2_iteration_para_dict['para'+str(i)].append(func2_iteration_para[i])

    #This creates a dictionary for func1 iteration parameters to check for convergence:
    func1_iteration_para_dict = {}
    for i in range(0, len(func1_iteration_para)):
        func1_iteration_para_dict['para'+str(i)] = []

    #This puts the first guessed value(s) in the dicts:
    for i in range(0, len(func1_iteration_para)):
        	func1_iteration_para_dict['para'+str(i)].append(func1_iteration_para[i])


    iteration_range = list(range(0, max_iter))
    Conv_list_iter = [0] #0 = guessed values

    for j in iteration_range:

        #This counts the iterations
        Conv_list_iter.append(j+1)

        #This calculates the first parameter(s)
        x = func1()



        #This puts the parameter in the desired dictionary to check for convergence:
        for i in range(0, len(func2_iteration_para)):
        	func2_iteration_para_dict['para'+str(i)].append(x.get(Key_func1[i]))

        #This updates the parameter so that the second function uses the new value(s):
        for i in range(0, len(func2_iteration_para)):
            func2_iteration_para[i] = float(x.get(Key_func1[i]))

        #This calculates the new iteration parameters of func2
        y = func2()



        #This puts the values in the desired dictionary to check for convergence:
        for i in range(0, len(func1_iteration_para)):
            	func1_iteration_para_dict['para'+str(i)].append(func1_iteration_para[i])

        #This updates the parameter for the next round of iteration:
        for i in range(0, len(func1_iteration_para)):
            func1_iteration_para[i] = y.get(Key_func2[i])

        #This checks if convergence is reached:
        if abs(func2_iteration_para_dict['para0'][j+1]-func2_iteration_para_dict['para0'][j]) <= delta:
                break

    Result_Dict = collections.OrderedDict()
    Result_Dict['Number_of_iterations'] = Conv_list_iter
    Result_Dict['Iteration_parameter_func1'] = func1_iteration_para_dict
    Result_Dict['Iteration_parameter_func2'] = func2_iteration_para_dict

    if additional_output != None:
        for i in range(0, len(additional_output)):
            Result_Dict['additional_output'+str(i)] = y.get(additional_output[i])
    return Result_Dict


def get_STH_eff(T_out, OER_catalyst_para, HER_catalyst_para, Electrolyte_para,
                Configuration_para, C, max_iter, delta,
                j_op_guess, E_op_guess,
                t, abso,
                I0,
                e_solarcell ,
                area_housing,
                e_housing , h_solarcell_air, h_housing_air,  E_th = 1.48,
                l_Ins = None, k_Ins = None,
                gaps = None, nid = None, r_ind = None,
                spectrum=am15g, T_ref = None, Isc_Tref = None,
                Isc_T_coeff = None, U_oc_Tref = None, U_oc_T_coeff = None,
                method = None, thermal_coupling = None):


    """Calculates the temperature-dependent STH efficiency of a thermally
       coupled or decoupled water splitting device based on the
     temperature-dependent catalyst and solar cell performance. Since the device
     temperature depends on the operation current density (et vice versa), this
     function uses an iterative method to compute the STH-efficiency
     employing the "itertation_func_general" function.


      In the thermally coupled device design, the function assumes that the
      temperature of the electrolyte is equal to the operation temperature of
      the solar cells which is denoted as the device temperature.In the
      thermally decoupled device design, the function only estimates the operation
     temperature of the solar cell assuming that the electrolyte temperature is
     equal to the outdoor temperature (i.e., the sun and an operation potential
     higher than the thermoneutral voltage does not heat up the electrolyte).
     In this case, device temperature describes the solar cell temperature.

        This function allows different methods for getting the IV curve of the
        solar cell:

            "datasheet": Estimates IV curve based on the manufacturer's
            datasheet (see estimate_JV_from_datasheet function)

            "det_balance": Calcuates IV curve based on the detailed balance
            limit (see get_JV_multijunction_det_balance function)

    Returns the STH-efficiency, IV curve for the catalysts  and solar cells,
    and the device temperature after convergeence is reached in an ordered
    dictionairy containing the keys:

        * STH - STH-efficiency in [%]
        * IV_catalysts - IV characeristics of the catalyts. List of two lists:
            Voltage [V] and Current [A] (absolute current)
        * IV_solar_cells - IV characeristics of the solar cell. List of two
            lists: Voltage [V] and Current [A/cm²]
        * T_device = Device temperature [K]

       Parameters
       ----------
       T: float
           Electrolyte Temperature in K

       OER_catalyst_para: list of floats
           OER Catalyst parameters: [j0_OER_Tref: exchange current density
                                     in Acm^-2,
                                     Tref_OER: Temperature in K,
                                     Ea_OER: Activation Energy OER in J/M,
                                     alpha_a_OER: Anodic Charge transfer
                                     coefficient]

       HER_catalyst_para: list of floats
           HER Catalyst parameters: [j0_HER_Tref: exchange current density
                                     in Acm^-2,
                                     Tref_HER: Temperature in K,
                                     Ea_HER: Activation Energy OER in J/M,
                                     alpha_a_HER: Cathodic charge transfer
                                     coefficient]

       Electrolyte_para: list of floats
           Electrolyte parameter: [pH: pH value of electrolyte,
                                   y0: y-intercept,
                                   m: slope (Temperature-dependence
                                   electrolyte according to
                                   Conductivity = y0 + m*Temperature [S/cm])]


       Configuration_para: List of floats
             Configuration parameter: [area_solar_cell: Area of solar cells in
                                     m^2,
                                     area_ratio: Ratio of Area catalysts/Area
                                     solar cells,
                                     distance: distance of electrodes in cm,
                                     geometry_const: geometry constant of the
                                     cell accounting for fringing
                                     effects with respect to the iR-drop
                                     in the electrolyte ( ≥1; 1 for no
                                     fringing effects)
        C = float
            Concentration factor []

        t: float
            Transmissivity of the optical collector []

        abso: float
            Surface absorptivity of the cell []

        I0: float
            Irradiance [W/m²] (1000 W/m² for AM1.5)

        E_th: float
            Thermoneutral Potential for water splitting [V]

        e_solarcell: float
            Emmisivity of the solar cells []

        area_housing: float
            Area of the device housing [m²]

        e_housing: float
            Emmisivity of the device housing or insulation (radiant barrier) in
            the infrared regiono [].

        h_solarcell_air: float
            Convective transfer coefficient solar cell - air [W/(m²*K)]

        h_housing_air: float
            Convective transfer coefficient device housing - air [W/(m²*K)]

        l_Ins: float
            Thickness of Insulation around the housing [m]

        k_Ins: float
            Thermal conductivity of Insulation around the housing [W/(m*K)]


       iteration: boolian
            If set to "True", electrolyte/device temperature is solved iteratively

       max_iter: integer
            Maximal number of iteration until convergence is reached

       delta: float
          Defines when convergence is reached and the loop is stopped

       j_op_guess: float
         First guess of the operation current density in A/m²
         (150 is a good start)

       E_op_guess: float
         First guess of the operation Voltage in [V]
         (2.2 is a good start)


       method: string ("datasheet" or "det_balance")
           Decides which method is used to get the IV curve of the solar cell:
            Can be "datasheet" (estimate_JV_from_datasheet-function) or
            "det_balance" (get_JV_multijunction_det_balance-function)


        Input parameters required for the det_balance method:

        gaps = list of floats
           bandgap of the absorbers (from the highest to the lowest gap)

       spectrum: solspec
           The spectrum.

       r_ind: list of floats

           Refractive indices of medium over and under the junction [n_o, n_u]

       Input parameters required for the datasheet method:
       T_ref = float
            Reference temperature in [K] of the T-coefficients of the the
            short current density
            and open circuit potential
       Isc_Tref = float
            Short current density in [mA/cm²] at the reference temperature
       Isc_T_coeff = float
            Temperature coefficient [%/K] of the short current density
       U_oc_Tref = float
            Open circuit voltage in [V] at the reference temperature
       U_oc_T_coeff = float
             Temperature coefficient [%/K] of the open circuit voltage
       nid = float
            diode ideality factor (between 1 and 2); Can be estimated from
            the open circuit
            potential as a function of light from the datasheet


       iteration: boolian
          If set to "True", electrolyte/device temperature is solved iteratively

       max_iter: integer
         Maximal number of iteration until convergence is reached

       delta: float
          Defines when convergence is reached and the loop is stopped


      j_op_guess: float
         First guess of the operation current density in A/m²
         (150 is a good start)

      E_op_guess: float
         First guess of the operation Voltage in [V]
         (2.2 is a good start)

      thermal_coupling: boolian
          Decides whether the system is thermally coupled or not
          (see above and "calc_device_t"-function for definitons)

       """

    if thermal_coupling is True:

        Iteration_parameter_func1 = [j_op_guess, E_op_guess]
        Iteration_parameter_func2 = [0]
        My_iter = itertation_func_general(
                    lambda: calc_device_t(T_out, t, abso,
                    Configuration_para[0], C, I0,
                    Iteration_parameter_func1[0], Iteration_parameter_func1[1], e_solarcell,
                    area_housing,
                    e_housing, h_solarcell_air,  h_housing_air, E_th, l_Ins, k_Ins),Iteration_parameter_func1,
                    lambda:match_catalysts_solar_cell_current(Iteration_parameter_func2[0],
                    OER_catalyst_para,
                    HER_catalyst_para, Electrolyte_para,
                    Configuration_para, C, T_ref = T_ref,  Isc_Tref = Isc_Tref,
                    Isc_T_coeff = Isc_T_coeff, nid = nid, U_oc_Tref = U_oc_Tref,
                    U_oc_T_coeff = U_oc_T_coeff, method = method),
                    Iteration_parameter_func2, max_iter, delta,
                    Key_func1 = ['T_device'],
                    Key_func2 = ['j_op', 'V_op'], additional_output = ['IV_catalysts', 'IV_solar_cells'])

        E0_OER_T = 1.23 + (My_iter['Iteration_parameter_func2']['para0'][-1]-298.15)*-0.0008456 #Temperature dependence E0_OER
        E0_HER_T = 0
        E_OER_N_T = E0_OER_T + ((phys_const.R*My_iter['Iteration_parameter_func2']['para0'][-1])/(4*phys_const.value
                                                                                                  ('Faraday constant')))*-2.303*4*Electrolyte_para[0]  #[V]
        E_HER_N_T = E0_HER_T + ((phys_const.R*My_iter['Iteration_parameter_func2']['para0'][-1])/(2*phys_const.value
                                                                                                  ('Faraday constant')))*-2.303*2*Electrolyte_para[0]  #[V]
        STH = (100*My_iter['Iteration_parameter_func1']['para0'][-1]*(E_OER_N_T - E_HER_N_T)) / (C*1000)

    elif thermal_coupling is False:

        E0_OER_T = 1.23 + (T_out-298.15)*-0.0008456 #Temperature dependence E0_OER
        E0_HER_T = 0
        E_OER_N_T = E0_OER_T + ((phys_const.R*T_out)/(4*phys_const.value
                                                  ('Faraday constant')))*-2.303*4*Electrolyte_para[0]  #[V]
        E_HER_N_T = E0_HER_T + ((phys_const.R*T_out)/(2*phys_const.value
                                                  ('Faraday constant')))*-2.303*2*Electrolyte_para[0]  #[V]


        Iteration_parameter_func1 = [j_op_guess, E_op_guess]
        Iteration_parameter_func2 = [0]

        My_iter = itertation_func_general(
                    lambda: calc_device_t(T_out, t, abso,
                    Configuration_para[0], C, I0,
                    Iteration_parameter_func1[0], Iteration_parameter_func1[1], e_solarcell,
                    area_housing,
                    e_housing, h_solarcell_air, h_housing_air, thermal_coupling = thermal_coupling),Iteration_parameter_func1,
                    lambda:match_catalysts_solar_cell_current(T_out,
                    OER_catalyst_para,
                    HER_catalyst_para, Electrolyte_para,
                    Configuration_para, C, T_ref = T_ref,  Isc_Tref = Isc_Tref,
                    Isc_T_coeff = Isc_T_coeff, nid = nid, U_oc_Tref = U_oc_Tref,
                    U_oc_T_coeff = U_oc_T_coeff, method = method, thermal_coupling = thermal_coupling,
                                       T_solarcell = Iteration_parameter_func2[0]),
                    Iteration_parameter_func2, max_iter, delta,
                    Key_func1 = ['T_device'],
                    Key_func2 = ['j_op', 'V_op'], additional_output = ['IV_catalysts', 'IV_solar_cells'])
        STH = (100*My_iter['Iteration_parameter_func1']['para0'][-1]*(E_OER_N_T - E_HER_N_T)) / (C*1000)


    Result_Dict = collections.OrderedDict()
    Result_Dict['STH'] = STH
    Result_Dict['IV_catalysts'] = My_iter['additional_output0']
    Result_Dict['IV_solarcells'] = My_iter['additional_output1']
    Result_Dict['T_device'] = My_iter['Iteration_parameter_func2']['para0'][-1]
    return Result_Dict
